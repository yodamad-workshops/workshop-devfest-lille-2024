# 🗼 Workshop Devoxx

ℹ️ Pour ce workshop, vous pourrez faire l'ensemble des exercices depuis le `WebIDE` / `CI/CD Editor` fournit par GitLab, ou via GitPod si vous préférez (ou sur votre poste si vraiment vous voulez). On vous conseille d'ouvrir plusieurs fenetres dans votre navigateur, une avec la home page du projet, une autre avec `WebIDE`, et une autre avec `CI/CD Editor`.

## Connexion

Rendez-vous sur [🦊 Gitlab](gitlab.com) et au choix :
- connectez vous avec votre compte si vous en avez déjà un, 👍
- créez un compte avec le mode d'authentification de votre choix, 🆕

## Le TP
Il y a plusieurs parties distinctes sur ce TP dont les énnoncés se trouvent respectivement dans les fichiers suivant : 
- [1_HelloWorld](./1_HelloWorld.md) : Pour se familiariser avec les notions d'Issues, de MergeRequest, de CI et les Pages ... 🌐
- [2_Web-App](./2_Web-App.md) : Pour l'utilisation de composants tiers à ajouter sur votre CI pour la gestion des TU et Qualité de code 🗠
- [3_Web-API](./3_Web-API.md) : La troisieme partie aborde la création d'images docker, les tests d'intégration,scan de sécurité, scheduling, runner custom, la CI qui génére la CI 🔁
- [4_Kubernetes](./4_Kubernetes.md) : Dernière partie pour jouer avec l'intégration Kubernetes dans GitLab. *Cette partie nécessite des outils en local sur votre poste*

Chaque partie dure environ 1h.

A vous de jouer ⛳️
